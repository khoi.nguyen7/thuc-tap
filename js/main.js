$(function () {
  let canTransparent = true;
  let currentTransparent = window.scrollY / 500;
  let scrollToTopShow = false;
  let bookingData = ["", "", ""];
  let data = {
    tabs: [
      {
        name: "Residential Property",
        imgs: [
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
        ],
      },
      {
        name: "Commercial Property",
        imgs: [
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
        ],
      },
      {
        name: "Commercial Property",
        imgs: [
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image.png",
        ],
      },
      {
        name: "Agriculture Property",
        imgs: [
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image (1).png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image (1).png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image (1).png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image (1).png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image (1).png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/Image (1).png",
        ],
      },
      {
        name: "Industrial Property",
        imgs: [
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
          "https://khoinguyen002.github.io/Product/jsonServer/assets/featureImg.png",
        ],
      },
    ],
  };

  const menu = $(".feature__drop .dropdown-menu");
  const tabsDefault = $(".feature__tabs");
  const dropTabs = $(".feature__drop");
  const dropBtn = $(".feature__drop button");
  const feature_imgs = $(".feature__imgs");

  const tabDefault = $(".feature__tab");

  const serviceDfTab = $(".service01__location span");
  const serviceSlider = $(".service__slider");
  const serviceDropBtn = $(".service01__drop button");
  const serviceDropItem = $(".service01__drop .dropdown-menu li a");

  const gap = 130;

  let locationModal = new bootstrap.Modal(
    document.getElementById("locationModal")
  );
  let checkModal = new bootstrap.Modal(document.getElementById("checkModal"));
  let resultModal = new bootstrap.Modal(document.getElementById("resultModal"));

  // <-------------------------- BEGIN Declare Function ------------------------->
  const handleOpacity = (opacity) => {
    $(".navbar").css({
      "background-color": `rgba(215, 215, 255, ${opacity})`,
      "box-shadow": `0 2px 50px 0 rgb(110 147 167 / ${opacity * 100}%)`,
    });
  };

  const handleScroll = () => {
    if (window.scrollY > 500 && !scrollToTopShow) {
      scrollToTopShow = true;
      $(".stt").fadeIn(500);
    } else if (window.scrollY <= 500 && scrollToTopShow) {
      scrollToTopShow = false;
      $(".stt").fadeOut(500);
    }
    handleOpacity(window.scrollY / 500);
  };

  const handleClick = function () {
    const itemId = $(this).attr("data-id");

    // update tab
    tabDefault.removeClass("featureActive");
    $(`.feature__tab[data-id=${itemId}]`).addClass("featureActive");

    // update dropbtn
    dropBtn.text($(`.feature .dropdown-item[data-id=${itemId}]`).text());
    // update slider
    $(".slider").hide();
    $(`.slider[data-id=${itemId}]`).show();

    // refresh slick
    $(`.slider[data-id=${itemId}]`).slick("refresh");
  };

  const handleServiceClick = function () {
    const itemId = $(this).attr("data-id");

    // update tabs
    $(".service01__location span").removeClass("serviceActive");
    $(`.service01__location span[data-id=${itemId}]`).addClass("serviceActive");

    //update dropbtn
    serviceDropBtn.text($(this).text());

    // update slider
    serviceSlider.hide();
    $(`.service__slider[data-id=${itemId}]`).show();

    // refresh slick
    $(`.service__slider[data-id=${itemId}]`).slick("refresh");
  };

  const handleShowCheckModal = function () {
    checkModal.show();
    const title =
      $(this).prev().text() == "Check in" ? "Check-In" : "Check-Out";
    $(".hero__subMenu__modal-check .modal-title").text(title);
    $("#checkModal").on("shown.bs.modal", function () {
      $("#datepicker")[0].focus();
    });
  };

  const handleNavScroll = function () {
    const scrollY = window.scrollY;
    const service01ToTop = $(".service01")[0].offsetTop;
    const testimonialToTop = $(".testimonial")[0].offsetTop;
    const featureToTop = $(".feature")[0].offsetTop;

    if (scrollY >= $(".feature")[0].offsetHeight + featureToTop - gap) {
      activeNavbar(4);
    } else if (scrollY >= featureToTop - gap) {
      activeNavbar(3);
    } else if (scrollY >= testimonialToTop - gap) {
      activeNavbar(2);
    } else if (scrollY >= service01ToTop - gap) {
      activeNavbar(1);
    } else {
      activeNavbar(0);
    }
  };

  const handleValidate = function (value, message, element) {
    if (!value) {
      $(`${element} .modal-body > span`).text(message);
      return false;
    }
    return true;
  };
  const clearError = function () {
    $(".modal-body > span").text("");
  };

  const activeNavbar = function (index) {
    $(".hero__menu__link.linkActive").removeClass("linkActive");
    $(".hero__menu__link")[index].classList.add("linkActive");
  };

  // <-------------------------- END Declare Function ------------------------->

  // <------------------------ BEGIN Declare Implement Function ------------------------>
  function settingScrollEffect() {
    $(".navbar").css({
      "background-color": `rgba(215, 215, 255, ${currentTransparent})`,
      "box-shadow": `0 2px 50px 0 rgb(110 147 167 / ${
        currentTransparent * 100
      }%)`,
    });
    $(".stt").click(() => {
      window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    });
    $(".navbar-toggler").click(() => {
      canTransparent = !canTransparent;
      if (!canTransparent) {
        handleOpacity(1);
      } else {
        handleOpacity(currentTransparent);
      }
    });
  }

  function preventMobileEffect() {
    if (window.innerWidth > 410) {
      Splitting();
      ScrollOut({
        targets: "[data-splitting]",
      });
    }
  }

  function appendFeatureSlider() {
    dropBtn.text(data.tabs[0].name);
    menu.append(
      data.tabs
        .map(
          (item, index) =>
            `<li><a class="dropdown-item" data-id=${index} href="#feature">${item.name}</a></li>`
        )
        .join("")
    );

    tabsDefault.append(
      data.tabs
        .map(
          (item, index) =>
            `<div class="feature__tab ${
              index == 0 ? "featureActive" : ""
            }" data-id=${index}>${item.name}</div>`
        )
        .join("")
    );

    feature_imgs.append(
      data.tabs
        .map(
          (item, index) =>
            `<div class="slider mb-5" data-id="${index}">
            ${item.imgs
              .map(
                (img) => `
              <div class="feature__img">
                <img
                  src="${img}"
                  alt=""
                />
                <div class="feature__img__btn"><span>Featured</span></div>
                <div class="feature__img__btn"><span>3D</span></div>
              </div>`
              )
              .join("")}
          </div>`
        )
        .join("")
    );
  }
  function showCondition() {
    if (data.tabs.length > 4) {
      tabsDefault.hide();
      dropTabs.show();
    }
    if (serviceSlider.length > 4) {
      $(".service01__drop").show();
      serviceDfTab.hide();
    }
  }

  function handleToggleSubMenu() {
    $(".hero__subBtn button").click(function () {
      $(".hero__subMenu__bg").show();
    });
    $(".hero__subMenu__bg").click(function () {
      $(".hero__subMenu__bg").hide();
    });
    $(".hero__subMenu").click(function (event) {
      event.stopPropagation();
    });
  }

  function handleClickNavLink() {
    $(".hero__menu__link").click(function () {
      window.scroll(0, $(`#${$(this).text()}`)[0].offsetTop - gap + 1);
    });
    $(".hero__menu__button button").click(function () {
      location.replace("/Product/thuc-tap/signUp_logIn.html");
      localStorage.setItem("isLogIn", false);
    });
  }

  function handleToggleModal() {
    $(".hero__subMenu div:first-child div:last-child").click(function () {
      $("#locationModal .modal-body").prepend($("#map"));
      locationModal.show();
    });

    $(".hero__subMenu div:nth-child(2) div:last-child").click(
      handleShowCheckModal
    );

    $(".hero__subMenu div:nth-child(3) div:last-child").click(
      handleShowCheckModal
    );
  }

  function handleSubmitModal() {
    $(".hero__subMenu__modal-location .modal-footer button:last-child").click(
      function () {
        if (
          handleValidate(
            bookingData[0],
            "Your destination cannot be empty!",
            "#locationModal"
          )
        ) {
          $(".hero__subMenu__location div:last-child").text(bookingData[0]);
          clearError();
          locationModal.hide();
        }
      }
    );

    $("#checkModal .modal-footer button:last-child").click(function () {
      let isCheckIn = $("#checkModal .modal-title").text() == "Check-In";
      const inputValue = $("#datepicker").val();
      if (
        handleValidate(
          inputValue,
          `Your Check-${isCheckIn ? "In" : "Out"} day cannot be empty!`,
          "#checkModal"
        )
      ) {
        $(
          `.hero__subMenu__check${isCheckIn ? "In" : "Out"} div:last-child`
        ).text(inputValue);
        checkModal.hide();
        clearError();
        $("#datepicker").val("");
        if (isCheckIn) {
          bookingData[1] = inputValue;
        } else bookingData[2] = inputValue;
      }
    });
  }

  function handleSubmitBooking() {
    $(".hero__subMenu__btn button").on("click", function () {
      resultModal.show();
      console.log($("#resultModal .modal-body > div"));
      $("#resultModal .modal-body > div span").each(function (index, item) {
        value = bookingData[index];
        item.innerText = value ? value : "";
        console.log(item);
      });
      $(".hero__subMenu > div div:last-child")[0].innerText =
        "Where are you going?";
      $(".hero__subMenu > div div:last-child")[1].innerText = "Add dates";
      $(".hero__subMenu > div div:last-child")[2].innerText = "Add dates";
    });
  }

  function initMap() {
    var map = L.map("map").setView([0, 0], 2);
    L.tileLayer("https://{s}.tile.osm.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
      center: [20.5937, 78.9629],
      zoom: 5,
      zoomControl: true,
      trackResize: true,
    }).addTo(map);
    L.Control.geocoder().addTo(map);
    map.on("popupopen", function (e) {
      $(".leaflet-control-geocoder-alternatives li").click(function () {
        $("#locationModal .modal-body div > span").text($(this).text().trim());
        bookingData[0] = $(this).text().trim();
      });
    });
  }

  function initSlick() {
    $(".slider").slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            arrows: false,
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
          },
        },
      ],
    });
    $(`.slider:not([data-id=0])`).hide();

    $(".service__slider").slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            arrows: false,
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
          },
        },
      ],
    });
    $(".service__slider:not([data-id=0])").hide();

    $(".testimonial__cards").slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            arrows: false,
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 1,
          },
        },
      ],
    });
  }

  function addSliderEvent() {
    tabDefault.click(handleClick);
    $(".feature__drop .dropdown-menu li a").click(handleClick);

    serviceDropItem.click(handleServiceClick);
    serviceDfTab.click(handleServiceClick);
  }

  function handleScrollEvent() {
    window.onscroll = () => {
      currentTransparent = window.scrollY / 500;
      if (canTransparent) {
        handleScroll();
      }
      handleNavScroll();
    };
  }

  function configBtn() {
    const isLogIn = JSON.parse(localStorage.getItem("isLogIn"));
    const btn = $(".hero__menu__button button");
    isLogIn ? btn.text("Log Out") : btn.text("Log In");
  }

  // <------------------------ END Declare Implement Function ------------------------>

  // <--------------------------- BEGIN Imple Function ---------------------------->
  appendFeatureSlider();
  initMap();
  initSlick();
  showCondition();
  addSliderEvent();
  settingScrollEffect();
  preventMobileEffect();
  handleToggleSubMenu();
  handleClickNavLink();
  handleToggleModal();
  handleSubmitModal();
  handleSubmitBooking();
  handleScrollEvent();
  configBtn();
  // <--------------------------- END Imple Function ---------------------------->
});
