$(function () {
  const loading = async function () {
    return await new Promise((resolve, reject) => {
      setTimeout(() => {
        $("button[type=submit]").attr({
          disabled: true,
        });
        $("button[type=submit]").html(`Please wait... <svg
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          class="bi bi-arrow-counterclockwise rotate"
          viewBox="0 0 16 16"
        >
          <path
            fill-rule="evenodd"
            d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"
          />
          <path
            d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"
          />
        </svg>`);
      });
      setTimeout(() => {
        $("button[type=submit]").attr({
          disabled: false,
        });
        $("button[type=submit]").text("Continue");
      }, 2500);
      setTimeout(resolve, 2500);
    });
  };

  const clearError = (id) => {
    $(`${id} div.error`).hide();
  };

  const catchError = (id, message) => {
    $(`${id} div.error`).text(message).show();
  };

  const isValidEmail = function (listAccount, email) {
    return listAccount.some((acc) => acc.email === email);
  };

  const checkAccount = function (listAccount, email, password) {
    return listAccount.some(
      (acc) => acc.email === email && acc.password === password
    );
  };

  const getAccountList = function () {
    const data = localStorage.getItem("listAccount");
    const listAccount = data ? JSON.parse(data) : [];
    return listAccount;
  };

  const storeAccount = function (email, password) {
    const listAccount = getAccountList();
    loading().then(() => {
      if (isValidEmail(listAccount, email)) {
        catchError("#signUp", "Email already exists! Try another one.");
      } else {
        listAccount.push({
          email: email,
          password: password,
        });
        localStorage.setItem("listAccount", JSON.stringify(listAccount));
        clearError("#signUp");
        new bootstrap.Tab($("button.nav-link")[0]).show();
      }
    });
  };

  function showTab() {
    $(".title div span").click(function () {
      new bootstrap.Tab($("button.nav-link")[1]).show();
    });
    $("button[type=reset]").click(function () {
      new bootstrap.Tab($("button.nav-link")[0]).show();
    });
  }

  function handleSignUp() {
    const email = $("form#signUp input[type=email]").val();
    const password = $("form#signUp input[name=password]").val();
    const passwordConfirmation = $(
      "form#signUp input[name=passwordConfirmation]"
    ).val();
    storeAccount(email, password);
  }

  function handleLogIn() {
    const email = $("form#logIn input[type=email]").val();
    const password = $("form#logIn input[type=password]").val();
    const listAccount = getAccountList();
    if (checkAccount(listAccount, email, password)) {
      loading().then(() => {
        clearError("#logIn");
        localStorage.setItem("isLogIn", true);
        location.replace("/Product/thuc-tap/");
      });
    } else {
      loading().then(() => {
        catchError("#logIn", "Email or password is invalid!");
      });
    }
  }

  function validateForm() {
    $("form#logIn").validate({
      rules: {
        username: {
          required: true,
        },
        password: {
          required: true,
        },
      },
      submitHandler: handleLogIn,
    });

    $("form#signUp").validate({
      rules: {
        username: {
          required: true,
        },
        password: {
          required: true,
          validatePassword: true,
        },
        passwordConfirmation: {
          required: true,
          equalTo: "#signUp input[name=password]",
        },
        checkBox: {
          required: true,
        },
      },
      messages: {
        passwordConfirmation: {
          required: "This field is required",
          equalTo: "Password does not match!",
        },
      },
      submitHandler: handleSignUp,
    });

    $.validator.addMethod(
      "validatePassword",
      function (value) {
        return RegExp(
          /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
        ).test(value);
      },
      "You must fill correct form of password!"
    );
  }

  function addEventSubmit() {
    $("form").submit((e) => {
      $("#checkBox-error").insertAfter(".form-check");
      e.preventDefault();
    });
  }

  function addEventInput() {
    $("#signUp input[name=password]").on("input", function () {
      validatePassword($(this).val());
    });
  }

  function validatePassword(password) {
    let matchedCase = new Array();
    let strongLevel = 0;

    matchedCase.push("[#?!@$%^&*-]"); // Special Charector
    matchedCase.push("[A-Z]"); // Uppercase Alpabates
    matchedCase.push("[0-9]"); // Numbers
    matchedCase.push("[a-z]"); // Lowercase Alphabates

    const activeStrength = function (nth, color) {
      for (let i = 1; i <= nth; i++) {
        $(`.strength-item:nth-child(${i})`).css({
          backgroundColor: color,
        });
      }
      for (let i = nth + 1; i <= 4; i++) {
        $(`.strength-item:nth-child(${i})`).css({
          backgroundColor: "#c5cdff",
        });
      }
    };

    matchedCase.forEach((_, i) => {
      strongLevel = i == 0 ? 0 : strongLevel;
      if (new RegExp(matchedCase[i]).test(password)) {
        strongLevel++;
      }
    });

    switch (strongLevel) {
      case 0:
        $(`.strength-item`).css({
          backgroundColor: "#c5cdff",
        });
        break;
      case 1:
        activeStrength(strongLevel, "rgb(255 111 111)");
        break;
      case 2:
        activeStrength(strongLevel, "#ffae51");
        break;
      case 3:
        activeStrength(strongLevel, "rgb(255 248 108)");
        break;
      case 4:
        activeStrength(strongLevel, "#50cd89");
        break;
      default:
        break;
    }
  }

  validateForm();
  addEventSubmit();
  addEventInput();
  showTab();
});
